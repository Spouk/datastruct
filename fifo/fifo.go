//----------------------------------------------------------------------
// sync access fifo
//-----------------------------------------------------------------------
package fifo

import (
	"errors"
	"sync"
)

var (
	errorNoSpace = errors.New("queue is full")
)

func NewFifo(length int, rotation bool) *Fifo {
	return &Fifo{
		rotation: rotation,
		length:   length,
		stock:    make([]interface{}, length),
		placed:   0,
	}
}

type Fifo struct {
	mu       sync.Mutex
	rotation bool
	length   int
	placed   int
	stock    []interface{}
}

func (f *Fifo) Add(v interface{}) error {
	f.mu.Lock()
	defer f.mu.Unlock()
	if f.placed < f.length {
		if index := f.getIndexFree(); index >= 0 {
			f.stock[index] = v
			f.placed++
		}
	} else {
		if f.rotation {
			f.clearLast()
			f.stock[0] = v
			f.placed++
		} else {
			return errorNoSpace
		}
	}
	return nil
}
func (f *Fifo) getIndexFree() int {
	for x := len(f.stock) - 1; x >= 0; x-- {
		if f.stock[x] == nil {
			return x
		}
	}
	return -1
}
func (f *Fifo) clearLast() {
	if len(f.stock) > 1 {
		tmp := make([]interface{}, f.length)
		for x := 0; x < len(f.stock)-1; x++ {
			tmp[x+1] = f.stock[x]
		}
		f.stock = tmp
		f.placed--
	}
}
func (f *Fifo) Get() interface{} {
	f.mu.Lock()
	defer f.mu.Unlock()
	if len(f.stock) > 1 && f.placed >= 1 {
		v := f.stock[len(f.stock)-1]
		tmp := make([]interface{}, f.length)
		for x := 0; x < len(f.stock)-1; x++ {
			tmp[x+1] = f.stock[x]
		}
		f.stock = tmp
		f.placed--
		return v
	} else {
		return nil
	}
}
func (f *Fifo) Clear() {
	f.mu.Lock()
	defer f.mu.Unlock()
	f.stock = make([]interface{}, f.length)
}
func (f *Fifo) GetStock() []interface{} {
	f.mu.Lock()
	defer f.mu.Unlock()
	return f.stock
}
